FROM ubuntu:latest

RUN apt -y update && \
    apt install -y python3 python3-flask python3-requests python3-openssl curl && \
    apt clean all && \
    rm -rf /var/cache/apt

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

RUN mkdir -p /opt/kubefish/

# The stars in the command below will only copy those files if they exist
COPY app/kubefish.py app/cert.pem* app/cert.key* /opt/kubefish/

ADD app/templates /opt/kubefish/templates

WORKDIR /opt/kubefish/

CMD ["/opt/kubefish/kubefish.py"]
ENTRYPOINT ["/usr/bin/python3"] 