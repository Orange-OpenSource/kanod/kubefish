import argparse
import flask
import os
import subprocess

from werkzeug.http import parse_authorization_header

# BMC Username (default: "root" )
bmc_username = os.getenv("BMC_USERNAME", "root")
# BMC password (default: "secret")
bmc_password = os.getenv("BMC_PASSWORD", "$2y$10$oYsa6nCsPUifRFYgeklnjezhZFJzcSy9wfSBpwkjx1MOApQILynZW")

app = flask.Flask(__name__)

# @app.route('/redfish/v1/Systems/<serverId>', methods=['GET', 'PATCH'])
# def system_resource(serverId=None   ):
#     username, password = get_credentials(flask.request)
#     global bmc_host
#     if flask.request.method == 'GET':
#        return flask.render_template(
#            'fake_system.json',
#            power_state=power_state,
#         )
#     else:
#        app.logger.info('patch request') 
#        boot = flask.request.json.get('Boot')
#        if not boot:
#            return ('PATCH only works for Boot'), 400
#        if boot:
#            target = boot.get('BootSourceOverrideTarget')
#            mode = boot.get('BootSourceOverrideMode')
#            if not target and not mode:
#                return ('Missing the BootSourceOverrideTarget and/or '
#                        'BootSourceOverrideMode element', 400)
#            else:
#                app.logger.info('Running script that sets boot from VirtualCD once')
#                try:
#                    my_env = set_env_vars(bmc_ip, username, password)
#                    subprocess.check_call(['custom_scripts/bootfromcdonce.sh'], env=my_env)
#                except subprocess.CalledProcessError as e:
#                    return ('Failed to set boot from virtualcd once', 400)

#                return '', 204
           
@app.route('/redfish/v1/Systems/<serverId>/Actions/ComputerSystem.Reset',
           methods=['POST'])
def system_reset_action(serverId=None):
    username, password = get_credentials(flask.request)
    if verify_bmc_credentials(username, password):
        return ('Failed to authenticate, wrong BMC credentials', 401)
    reset_type = flask.request.json.get('ResetType')
    global power_state 
    if reset_type == 'On':
        app.logger.info('Running script that powers on the server')
        try:
#            command = ['virtctl', 'start', serverId]
            command = ['kubectl', 'patch', 'virtualmachine', serverId, '--type', 'merge', '-p', '\'{"spec":{"running":true}}\'']
            subprocess.run(command, check=True)
        except subprocess.CalledProcessError as e:
            return ('Failed to poweron the server', 400)
        power_state = 'On'
    else:
        app.logger.info('Running script that powers off the server')
        try:
#            command = ['virtctl', 'stop', serverId]
            command = ['kubectl', 'patch', 'virtualmachine', serverId, '--type', 'merge', '-p', '\'{"spec":{"running":false}}\'']
            subprocess.run(command, check=True)
        except subprocess.CalledProcessError as e:
            return ('Failed to poweroff the server', 400)
        power_state = 'Off'

    return '', 204

def get_credentials(flask_request):
    auth = flask_request.headers.get('Authorization', None)
    username = ''
    password = ''
    if auth is not None:
        creds = parse_authorization_header(auth)
        username = creds.username
        password = creds.password
    app.logger.debug('Returning credentials')
    app.logger.debug('Username: ' + username + ', password: ' + password)
    return username, password

def verify_bmc_credentials(username, password):
    return username == bmc_username and password == bmc_password

def run(port, debug, tls_enabled, cert_file, key_file):
    if tls_enabled:
        if os.path.exists(cert_file) and os.path.exists(key_file):
            app.run(host='::', port=port, debug=debug, ssl_context=(cert_file, key_file))
        else:
            app.logger.error('%s or %s not found.', cert_file, key_file)
            exit()
    else:
        app.run(host='::', port=port, debug=debug)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='KubeFish, a RedFish proxy that calls Kubevirt command to execute hardware actions.')
    parser.add_argument('--tls-enabled', action='store_true', help='Enable self-signed TLS mode with users configured cert and key files.')
    parser.add_argument('--cert-file', type=str, default='./cert.pem', help='Path to the certificate public key file. (default: %(default)s)')
    parser.add_argument('--key-file', type=str, default='./cert.key', help='Path to the certificate private key file. (default: %(default)s)')
    parser.add_argument('-r', '--remote-bmc', type=str, required=True, help='The BMC IP this KubeFish instance will connect to. e.g: 192.168.1.10')
    parser.add_argument('-p','--listen-port', type=int, required=False, default=9000, help='The port where this KubeFish instance will listen for connections.')
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    bmc_host = args.remote_bmc
    tls_enabled = args.tls_enabled
    port = args.listen_port
    debug = args.debug
    cert_file = args.cert_file
    key_file = args.key_file

    power_state = 'On'
    run(port, debug, tls_enabled, cert_file, key_file)